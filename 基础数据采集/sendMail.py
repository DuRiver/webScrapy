import smtplib
from email.mime.text import MIMEText
from bs4 import BeautifulSoup
from urllib.request import urlopen
import time

'''
msg = MIMEText("the body of the email is here")

msg['SUbject'] = "An email alert"
msg['From'] = "dcj1103@gmail.com"
msg['To'] = 'duchangjiang@outlook.com'

s = smtplib.SMTP('localhost')
s.send_message(msg)
s.quit()
'''


def senMail(subject, body):
    msg = MIMEText(body)
    msg['Subject'] = subject

    msg['From'] = "dcj1103@gmail.com"
    msg['To'] = 'duchangjiang@outlook.com'

    s = smtplib.SMTP('localhost')
    s.send_message(msg)
    s.quit()


bsObj = BeautifulSoup(urlopen("https://isitchristmas.com/"))
while(bsObj.find('a', {'id': 'answer'}).attrs['title'] == 'NO'):
    print("It is not Christmas yet.")
    time.sleep(3600)


bsObj = BeautifulSoup(urlopen("https://isitchristmas.com/"))
senMail("It's Christmas!", "According to https://itischristmas.com, it is Christmas!")
