from urllib.request import urlopen
from urllib.error import URLError
from bs4 import BeautifulSoup
import re
import json
import time
import datetime
import random


random.seed(datetime.datetime.now())


def getCountry(ipAddress):
    try:
        response = urlopen("http://freegeoip.net/json/" +
                           ipAddress).read().decode('utf-8')
    except URLError:
        return None
    responseJson = json.loads(response)
    return responseJson.get('country_code')


def getLinks(articleUrl):
    html = urlopen("http://en.wikipedia.org" + articleUrl)
    bsObj = BeautifulSoup(html)
    return bsObj.find("div", {"id": "bodyContent"}).findAll('a', href=re.compile('^(/wiki/)((?!:).)*$'))


def getHistoryIPs(pageUrl):
    # 历史页面url格式：https://zh.wikipedia.org/w/index.php?title=Python&action=history
    pageUrl = pageUrl.replace("/wiki/", "")
    historyUrl = "https://en.wikipedia.org/w/index.php?title=" + \
        pageUrl + "&action=history"
    print("history url is: " + historyUrl)
    html = urlopen(historyUrl)
    bsObj = BeautifulSoup(html)
    # 找出class属性为mw-userlink, mw-anonuserlink的链接
    # 它们用IP地址代替用户名
    ipAddresses = bsObj.findAll('a', {'class': 'mw-userlink mw-anonuserlink'})
    addressList = set()
    for ipAddress in ipAddresses:
        addressList.add(ipAddress.get_text())
    return addressList


links = getLinks("/wiki/Python_(programming_language)")
while(len(links) > 0):
    for link in links:
        print("----------------------------------------------------------")
        historyIPs = getHistoryIPs(link.attrs['href'])
        for historyIP in historyIPs:
            country = getCountry(historyIP)
            if(country is not None):
                print(historyIP + " is from " + country)
                time.sleep(1)
    newLink = links[random.randint(0, len(links) - 1)].attrs['href']
    links = getLinks(newLink)
