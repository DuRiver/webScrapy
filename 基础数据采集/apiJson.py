import json
from urllib.request import urlopen
import socket


def getCountry(ipAddress):
    response = urlopen("http://freegeoip.net/json/" +
                       ipAddress).read().decode('utf-8')
    responseJson = json.loads(response)
    return responseJson.get("country_code")


def get_host_ip():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(('8.8.8.8', 80))
        ip = s.getsockname()[0]
    finally:
        s.close()
    return ip


print(getCountry("107.178.194.249"))
